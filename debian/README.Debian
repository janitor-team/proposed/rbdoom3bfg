RBDoom3-BFG for Debian

Playing the original game
=========================

This source release does not contain any game data, the game data is still
covered by the original EULA and must be obeyed as usual.

You must patch the game to the latest version (Steam will do that for you)

Note that Doom 3 BFG Edition is available from the Steam store at
http://store.steampowered.com/app/208200/ or as a DVD edition.

However, the files on the DVD are encrypted, and you need Steam to decrypt them.
This can be done with wine. Just use the setup.exe on the DVD with wine, and let
steam finish up the installation and move the files to the right location.

The game will look for the data at:

 $HOME/.rbdoom3bfg/base
 /usr/share/games/doom3bfg/base

The README.txt has more information (Section 7)

The package game-data-packager(6) supports rbdoom3bfg to create convenience, easy
installable data packages and should also be able to update the data
automatically to the latest version.

For example, you can create your game data package with:

   game-data-packager [--no-search] doom3 [path-to-game-files ...]

This will also download patches to get your data to the latest version.  If it
cannot find the datafiles or takes ages, hint the location by adding the
directory to the commandline and not to search for the data (as indicated above
with the optional parameters in brackets)

Note: This process might take some time. Be patient!

Note: The game data for the original Doom 3 edition is not compatible with the
BFG edition of the game engine.


Free map data
=============

To qualify for the main section of Debian, some free map data needs to be
available, even if the data is only a proof-of-concept.

A testmap is available in the OpenTechBFG repository, available in the assets branch of
their git repo at https://github.com/OpenTechEngine/OpenTechBFG.

To see the demo, checkout the above branch, cd to it and start the program with
rbdoom3bfg +set fs_game base

Then, in the (empty SDL2) window open up the console (~ key) and type
map testmaps/test.map

(Thanks to Mikko Kortelainen of OpenTechBFG for the hint.)

 -- Tobias Frost <tobi@debian.org>  Sun, 15 Feb 2014 15:04:26 +0200
